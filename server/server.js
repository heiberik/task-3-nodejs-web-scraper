const express = require("express")
const app = express()
const scraper = require("./scraper")
const fs = require('fs');
const cors = require("cors")

app.use(cors())
app.use(express.static("client"))

let lastSearch = ""

app.get("/words", async (req, res) => {

    const lang = req.query.lang
    const search = req.query.search

    const newSearch = lang + search
    if (lastSearch === newSearch){
        return res.status(400).send()
    }
    lastSearch = newSearch

    await scraper.scrape(lang, search)
    let data = JSON.parse(fs.readFileSync('./wordsCount.json', 'utf8'));

    if (!Object.keys(data).length) return res.status(404).send()

    res.json(data)

})

const PORT = 3000
app.listen(PORT, () => console.log(`Server started on port ${PORT}`))

