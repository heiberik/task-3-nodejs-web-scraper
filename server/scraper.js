
const puppeteer = require("puppeteer")
const fs = require("fs")

const scrape = async (lang, search) => {

    const url = `https://${lang}.wikipedia.org/wiki/${search}`

    const browser = await puppeteer.launch()
    const page = await browser.newPage()
    await page.goto(url)
    const pTags = await page.$$('p');

    const wordCountList = {}

    for (const p of pTags) {

        const text = await page.evaluate(el => el.innerText, p);
        const words = text.split(" ")
        words.forEach(word => {
            
            let w = word.trim().replace(/[ ,.·():\n\t`"']/g, "").toString()
            w = w.toLowerCase()
            if (w !== "") {

                if (!wordCountList[w]){
                    wordCountList[w] = 1
                }
                else {
                    wordCountList[w] += 1
                }
            }         
        })
    }

    for (word in wordCountList){
        if (wordCountList[word] < 2){
            delete wordCountList[word]
        }
    }

    fs.writeFileSync("./wordsCount.json", JSON.stringify(wordCountList), err => {
        if (err) throw err
    })

    await browser.close();
}


module.exports.scrape = scrape 



