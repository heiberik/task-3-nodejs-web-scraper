const inputBox = document.getElementById("inputBox")
const searchButton = document.getElementById("searchButton")
const selectLang = document.getElementById("selLang")
const wordListDOM = document.getElementById("wordList")

let wordList = []

const search = () => {

    const text = inputBox.value
    const lang = selectLang.value

    fetch(`http://localhost:3000/words?lang=${lang}&search=${text}`)
        .then(res => {
            if (res.status === 200) return res.json()
            else throw new Error(res.status)
           
        })
        .then(json => {
            wordList = []
            for (const word in json){
                wordList.push({word: word, num: json[word]})
            }
            sortByNumber()
            renderPage()
        })
        .catch((e) => {
            
            if (e.message === "400"){
                // User search is the same. show error message or smth.
            }
            if (e.message === "404"){
                wordList = []
                wordList.push({word: "Bad search", num: 404})
                console.log(wordList)
                renderPage()
            }
        })
}



const renderPage = () => {

    const biggestNum = wordList[0].num

    const add =  50 / biggestNum

    wordListDOM.innerHTML = ""
    wordList.forEach(w => {
        const div = document.createElement("div")
        div.className = "word"
        div.style.width = 50 + (w.num * add) + "%"
        div.style.backgroundColor = getRandomColor()
        const p1 = document.createElement("p")
        p1.innerText = w.word
        const p2 = document.createElement("p")
        p2.innerText = w.num
        div.appendChild(p1)
        div.appendChild(p2)
        wordListDOM.appendChild(div)
    })

}

const getRandomColor = () => {
    var letters = '89ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 8)];
    }
    return color;
}

const sortByNumber = () => {
    wordList.sort((a, b) => { return b.num - a.num })
}

searchButton.addEventListener("click", search)